# Pausable Game Grid #

### Description ###

InteractiveGameGrid is exactly what it sounds like: it is a grid of clickable
objects that can be used for a simple game (a puzzle game, for example).  Each
square in the grid is clickable, so it is up to the user to determine what
action is supposed to occur when a square is clicked.

The grid also has a countdown timer, as well as pausing functionality.  When the
pause button is clicked, a menu is brought up, effectively pausing any
activities occurring within the game.

### API ###

InteractiveGameGrid was developed for API Level 15 (Android 4.0.3) and up.

### Developed By ###

Erin Sosebee
