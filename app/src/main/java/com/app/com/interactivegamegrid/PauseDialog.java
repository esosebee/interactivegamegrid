package com.app.com.interactivegamegrid;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

/**
 * The PauseDialog class creates a dialog fragment that is displayed on the screen when the user
 * clicks the Pause button.
 *
 * @author Erin Sosebee
 * @version 1.0
 * @since 2015-27-02
 */
public class PauseDialog extends DialogFragment implements OnClickListener
{
  Button resumeBtn, menuBtn;
  Communicator communicator; // Reference to Communicator interface inside this class

  /**
   * Assigns a reference of the activity to the Communicator interface.
   * @param activity
   */
  @Override
  public void onAttach(Activity activity)
  {
    super.onAttach(activity);
    communicator = (Communicator) activity;
  }

  /**
   * Creates the dialog view to be displayed on the screen by the GameActivity class.
   * @param inflater
   * @param container
   * @param savedInstanceState
   * @return View view
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.pause_dialog, null);

    // Removes title bar from dialog
    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

    // Prevents dialog from being cancelled if user clicks outside of dialog
    setCancelable(false);

    resumeBtn = (Button) view.findViewById(R.id.resumeGameButton);
    menuBtn = (Button) view.findViewById(R.id.endGameButton);
    resumeBtn.setOnClickListener(this);
    menuBtn.setOnClickListener(this);

    return view;
  }

  /**
   * Handles button clicks within the dialog.
   * @param v
   */
  @Override
  public void onClick(View v)
  {
    if (v.getId() == R.id.resumeGameButton)
    {
      communicator.onDialogMessage("go");
      dismiss(); // Dismisses the dialog
    }
    else if (v.getId() == R.id.endGameButton)
    {
      communicator.onDialogMessage("stop");
      // TODO: sends user back to the home screen
    }
  }

  /**
   * Interface that allows the GameActivity class and the PauseDialog class to interact with each
   * other.
   */
  interface Communicator
  {
    public void onDialogMessage(String message);
  }
}
