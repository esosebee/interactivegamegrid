package com.app.com.interactivegamegrid;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * The Tile class contains all properties that each tile on the game board
 * has by default.
 *
 * @author Erin Sosebee
 * @version 1.0
 * @since 2015-24-02
 */
public class Tile extends Button
{
  private boolean isOddColor;

  public Tile(Context context)
  {
    super(context);
  }

  public Tile(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }

  public Tile(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
  }

  /**
   * Sets the default color/other default properties of tiles on the board
   */
  public void setDefault()
  {
    isOddColor = false;
    // TODO: set color here?
//    this.setBackgroundResource(R.drawable.tile);
  }
}

