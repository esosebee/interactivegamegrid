package com.app.com.interactivegamegrid;

import android.app.FragmentManager;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

/**
 * The GameActivity class creates and displays the game board that the user will interact with.
 * The game board is created and formatted using a TableLayout.
 *
 * This class also interacts with the PauseDialog class to display a dialog fragment whenever the
 * user pauses the game.  Above the game board, two text views are displayed: a countdown timer
 * and the current level of the game that the user is on.
 *
 * @author Erin Sosebee
 * @version 1.0
 * @since 2015-24-02
 */
public class GameActivity extends ActionBarActivity implements PauseDialog.Communicator
{
  boolean DEBUG  = true;

  TableLayout gridBoard;
  Tile[][] tiles;

  int totalRows, totalCols;
  int tileWH = 5, tilePadding = 23;

  Timer timer;
  TextView timeText;
  long timeRemaining;

  ImageButton startPauseBtn;
  boolean gameRunning = true;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game);

    gridBoard = (TableLayout) findViewById(R.id.gameGrid);
    startPauseBtn = (ImageButton) findViewById(R.id.startPauseButton);

    timeText = (TextView) findViewById(R.id.timerText);
    timeText.setText("01:00");

    timer = new Timer(60000, 1000);
    timer.start();

    startPauseBtn.setOnClickListener(new OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        if (gameRunning == false)
        {
          timer = new Timer(timeRemaining, 1000);
          timer.start();
          gameRunning = true;
        }
        else
        {
          timer.cancel();
          gameRunning = false;
          showDialog(v);
        }
      }
    });

    // Draw the game board to the screen
    createGameBoard();
    showGameBoard();
  }

  /**
   * Displays a dialog fragment when the pause button is clicked.
   * @param v
   */
  public void showDialog(View v)
  {
    FragmentManager manager = getFragmentManager();
    PauseDialog pauseDialog = new PauseDialog();
    pauseDialog.show(manager, "PauseDialog");
  }

  /**
   * The message sent from the PauseDialog class based on which button the user clicked.
   * @param message
   */
  @Override
  public void onDialogMessage(String message)
  {
    if (message.equals("go"))
    {
      timer = new Timer(timeRemaining, 1000);
      gameRunning = true;
      timer.start();
    }
  }

  /**
   * Adds the created game board to the TableLayout and shows the game board on the screen.
   */
  public void showGameBoard()
  {
    // For every row
    for (int row = 0; row < totalRows; row++)
    {
      // Create a new table row
      TableRow tableRow = new TableRow(this);

      // Set the height and width of the row
      tableRow.setLayoutParams(new LayoutParams((tileWH * tilePadding) * totalCols,
          tileWH * tilePadding));

      // For every col
      for (int col = 0; col < totalCols; col++)
      {
        // Set width and height of tile
        tiles[row][col].setLayoutParams(new LayoutParams(tileWH * tilePadding,
            tileWH * tilePadding));

        // Add some padding to the tile
        tiles[row][col].setPadding(tilePadding, tilePadding, tilePadding, tilePadding);

        // Add the tile to the table row
        tableRow.addView(tiles[row][col]);
      }
      // Add the row to the game board layout
      gridBoard.addView(tableRow, new TableLayout.LayoutParams((tileWH * tilePadding) *
          totalCols, tileWH * tilePadding));
    }
  }

  /**
   * Creates the tiles to be displayed in the game board
   */
  public void createGameBoard()
  {
    totalRows = 9;
    totalCols = 9;

    // Set up the tiles array
    tiles = new Tile[totalRows][totalCols];

    for (int row = 0; row < totalRows; row++)
    {
      for (int col = 0; col < totalCols; col++)
      {
        // Create a tile
        tiles[row][col] = new Tile(this);

        // Set the tile to default
        tiles[row][col].setDefault();

        //  TODO: variables used for debugging (displaying Toast message when square is clicked)
        final int currentRow = row;
        final int currentCol = col;

        tiles[row][col].setOnClickListener(new OnClickListener()
        {
          @Override
          public void onClick(View v)
          {
            // TODO: click functionality here
            Context context = getApplicationContext();
            CharSequence text = "Clicked: (" + Integer.toString(currentRow) + "," +
                "" + Integer.toString(currentCol) + ")";
            int duration = Toast.LENGTH_LONG;

            Toast.makeText(context, text, duration).show();
          }
        });
      }
    }
  }

  public void endGame()
  {
    // Remove the table rows from the game board table layout
    gridBoard.removeAllViews();

    // Reset variables
    gameRunning = false;
  }

  /**
   * The Timer class creates a CountDownTimer
   */
  public class Timer extends CountDownTimer
  {
    public Timer(long millisInFuture, long countDownInterval)
    {
      super(millisInFuture, countDownInterval);
    }

    public void onTick(long millisUntilFinished)
    {
      timeRemaining = millisUntilFinished; // Keeps track of the time left on the timer

      // String to be displayed by the countdown timer's TextView
      String minSec = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(timeRemaining),
          TimeUnit.MILLISECONDS.toSeconds(timeRemaining) -
              TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeRemaining)));

      if (DEBUG) System.out.println(minSec);

      timeText.setText(minSec);
    }

    @Override
    public void onFinish()
    {
      timeText.setText("00:00");
    }
  }
}
